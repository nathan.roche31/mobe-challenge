package helloandroid.m2dl.mobe_challenge;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import helloandroid.m2dl.mobe_challenge.ui.game.GameActivity;
import helloandroid.m2dl.mobe_challenge.ui.photoActivity.PhotoActivity;

public class MainActivity extends AppCompatActivity {
    private AppCompatActivity me = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Play Button
        Button playButton = findViewById(R.id.play_button);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Load an activity
                final Intent intent = new Intent(me, GameActivity.class);
                intent.putExtra("playerNumber", 1);
                startActivity(intent);
            }
        });

        Button photoButton = findViewById(R.id.photo_button);
        photoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Load an activity
                final Intent intent = new Intent(me, PhotoActivity.class);
                intent.putExtra("playerNumber", 1);
                startActivity(intent);
            }
        });
    }
}
