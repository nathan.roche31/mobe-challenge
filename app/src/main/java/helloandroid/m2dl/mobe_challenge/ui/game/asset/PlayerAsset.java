package helloandroid.m2dl.mobe_challenge.ui.game.asset;

import android.graphics.Rect;
import android.os.Handler;

import helloandroid.m2dl.mobe_challenge.ui.game.GameController;

public class PlayerAsset extends Asset {

    private static final int TIME_SPEED = 2;
    private static final int PLAYER_SPEED = 1;

    private Handler handler = new Handler();
    private GameController gameController;

    private int jumpCount = 0;

    private boolean canJump;
    private boolean isFalling;
    private boolean isFinish;
    private boolean isWalking;
    /**
     * Gestion de la chute.
     */
    private Runnable fall = new Runnable() {
        public void run() {
            if (isFinish || gameController.isTouchEndFlag()) {
                return;
            }
            canJump = false;
            if (!gameController.isOnObstacle()) {
                if (gameController.isOnDeathZone()) {
                    killPlayer();
                } else {
                    setPositionY(getPositionY() + 1);
                    handler.postDelayed(fall, TIME_SPEED);
                }
            } else {
                canJump = true;
            }
        }
    };

    /**
     * Gestion du déplacement.
     */
    private Runnable walk = new Runnable() {
        public void run() {
            if (gameController.isTouchEndFlag()) {
                return;
            }
            if (!isFinish) {
                if (isWalking) {
                    if (!gameController.isNextToRightWall()) {
                        setPositionX(getPositionX() + PLAYER_SPEED);
                    }
                    if (!gameController.isOnObstacle()) {
                        canJump = false;
                        if (!isFalling) {
                            isFalling = true;
                            handler.postDelayed(fall, TIME_SPEED);
                        }
                    }
                }

                handler.postDelayed(walk, TIME_SPEED);
            }
        }
    };

    /**
     * Gestion du saut.
     */
    private Runnable jump = new Runnable() {
        public void run() {
            if (jumpCount < 35) {
                setPositionY(getPositionY() - 1);
                jumpCount++;
                // isOnFinishLine();
                handler.postDelayed(jump, TIME_SPEED);
            } else {
                jumpCount = 0;
                handler.postDelayed(fall, TIME_SPEED);
            }
        }
    };

    public PlayerAsset(GameController gameController) {
        super(AssetType.PLAYER);
        this.gameController = gameController;
        init();
    }

    /**
     * Initialisation du joueur.
     */
    private void init() {
        setPositionX(0);
        setPositionY(0);
        jumpCount = 0;
        canJump = true;
        isFinish = false;
        isWalking = false;
        isFalling = false;
    }

    /**
     * Mort du joueur.
     */
    public void killPlayer() {
        setPositionX(0);
        setPositionY(0);
        jumpCount = 0;
        canJump = false;
        isFinish = true;
        isWalking = false;
        isFalling = true;
    }

    public void jump() {
        handler.postDelayed(jump, TIME_SPEED);
    }

    public void startWalking() {
        handler.postDelayed(walk, PLAYER_SPEED);
    }


    public boolean isWalking() {
        return isWalking;
    }

    public void setWalking(boolean walking) {
        isWalking = walking;
    }

    public boolean canJump() {
        return canJump;
    }

    @Override
    public boolean isOnHitBox(Rect position) {
        return false;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }
}
