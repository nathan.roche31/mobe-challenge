package helloandroid.m2dl.mobe_challenge.ui.game.asset;

import android.graphics.Bitmap;
import android.graphics.Rect;

public abstract class Asset {
    private AssetType type;
    private float positionX;
    private float positionY;
    private Bitmap drawImage;

    public Asset(AssetType type) {
        this.type = type;
    }


    public boolean isOnHitBox(Rect position) {
        return position.intersect(myPositionToRect());
    }


    public Bitmap getDrawImage() {
        return drawImage;
    }

    public void setDrawImage(Bitmap drawImage) {
        this.drawImage = drawImage;
    }

    public AssetType getType() {
        return type;
    }

    public void setType(AssetType type) {
        this.type = type;
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public final Rect myPositionToRect() {
        return new Rect((int) positionX, (int) positionY, ((int) positionX) + drawImage.getWidth(), ((int) positionY) + getDrawImage().getHeight());
    }
}
