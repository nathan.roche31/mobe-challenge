package helloandroid.m2dl.mobe_challenge.ui.game.asset;

import android.graphics.Rect;

public class ObstacleAsset extends Asset {
    public ObstacleAsset() {
        super(AssetType.OBSTACLE);
    }
}
