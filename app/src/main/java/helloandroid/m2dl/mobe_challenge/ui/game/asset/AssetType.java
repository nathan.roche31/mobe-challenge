package helloandroid.m2dl.mobe_challenge.ui.game.asset;

public enum AssetType {
    PLAYER,
    OBSTACLE,
    END
}
