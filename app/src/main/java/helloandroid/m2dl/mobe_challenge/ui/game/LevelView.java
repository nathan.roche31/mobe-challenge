package helloandroid.m2dl.mobe_challenge.ui.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.LinkedList;
import java.util.Objects;

import helloandroid.m2dl.mobe_challenge.ui.game.asset.Asset;
import helloandroid.m2dl.mobe_challenge.ui.game.asset.AssetType;

public class LevelView extends View implements SensorEventListener {

    private Paint defaultPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int playerNumber;
    private LinkedList<Asset> shownAssets = new LinkedList<>();
    private GameController gameController = new GameController(this);

    public LevelView(Context context) {
        super(context);
        init(context);
        initLevel();
    }

    public LevelView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initLevel();
    }

    public LevelView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
        initLevel();
    }

    public LevelView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
        initLevel();
    }

    private void init(Context context) {
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void initLevel() {
        shownAssets.clear();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (Asset asset : shownAssets) {
            canvas.drawBitmap(asset.getDrawImage(), asset.getPositionX(), asset.getPositionY(), defaultPaint);
        }

        invalidate();
    }

    public void addAsset(Asset asset) {
        shownAssets.add(asset);
        if (asset.getDrawImage().getHeight() > getHeight() / 15) {
            asset.setDrawImage(resizeKeepingAspectRatio(asset.getDrawImage(), getHeight() / 15));

        }
        invalidate();

    }

    private Bitmap resizeKeepingAspectRatio(Bitmap image, int maxHeight) {
        float aspectRatio = image.getWidth() /
                (float) image.getHeight();
        int height = maxHeight;
        int width = Math.round(height / aspectRatio);

        return Bitmap.createScaledBitmap(image, width, height, false);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        gameController.initGame();
    }

    public void resetLevel() {
        initLevel();
    }

    public void clearAssetsWithTypes(final AssetType type) {
        for (final Asset asset : shownAssets) {
            if (type.equals(asset.getType())) {
                shownAssets.remove(asset);
            }
        }
    }

    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int eventaction = event.getAction();
        switch (eventaction) {
            case MotionEvent.ACTION_UP:
                gameController.startPlayerJump();
                break;
            default:
                break;
        }

        // tell the system that we handled the event and no further processing is required
        return true;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                // Axis of the rotation sample, not normalized yet.
                double axisX = sensorEvent.values[0];
                double axisY = sensorEvent.values[1];
                double axisZ = sensorEvent.values[2];

                double norm_Of_g = Math.sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ);

                axisX = axisX / norm_Of_g;

                int inclination = (int) Math.round(Math.toDegrees(Math.acos(axisX)));

                if (inclination > 0 && inclination < 30) {
                    Objects.requireNonNull(gameController).stopPlayerMovement();
                } else {
                    Objects.requireNonNull(gameController).movePlayerForward();
                }

                invalidate();
                break;
            default:
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }
}

