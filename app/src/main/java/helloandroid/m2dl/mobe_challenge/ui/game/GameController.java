package helloandroid.m2dl.mobe_challenge.ui.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import helloandroid.m2dl.mobe_challenge.R;
import helloandroid.m2dl.mobe_challenge.ui.game.asset.Asset;
import helloandroid.m2dl.mobe_challenge.ui.game.asset.EndAsset;
import helloandroid.m2dl.mobe_challenge.ui.game.asset.ObstacleAsset;
import helloandroid.m2dl.mobe_challenge.ui.game.asset.PlayerAsset;
import helloandroid.m2dl.mobe_challenge.utils.Utils;

public class GameController {
    private static final float PLAYER_START_X = 50;
    private static final float PLAYER_START_Y = 50;
    private int playerNumberTurn;
    private PlayerAsset player;
    private List<Asset> obstacles = new ArrayList<>();
    private EndAsset endFlag;
    private LevelView levelView;

    public GameController(LevelView levelView) {
        this.levelView = levelView;
    }

    private void createPlayer() {
        player = new PlayerAsset(this);
        player.setPositionX(PLAYER_START_X);
        player.setPositionY(PLAYER_START_Y);
        player.setDrawImage(Utils.getBitmapFromVectorDrawable(levelView.getResources().getDrawable(R.drawable.ic_robot)));
        levelView.addAsset(player);
    }

    public void initGame() {
        levelView.resetLevel();
        obstacles = new ArrayList<>();
        createPlayer();
        createStartPlateform();
        createEndPlateform();
        for (int i = 0; i < 6; i++) {
            createMorePlateform();
        }
        player.startWalking();
    }

    private void createMorePlateform() {
        Random rand = new Random();
        final Asset plateform = createPlatform(Math.abs(rand.nextInt() % 60) + 30);
        plateform.setPositionX(Math.abs(rand.nextInt() % (levelView.getWidth() - 1)) + 1);
        plateform.setPositionY(Math.abs(rand.nextInt() % (levelView.getHeight() - 1)) + 1);
        levelView.addAsset(plateform);
        obstacles.add(plateform);
    }

    private void createStartPlateform() {
        final Asset plateform = createPlatform(player.getDrawImage().getWidth() * 5);
        plateform.setPositionX(player.getPositionX() + 1);
        plateform.setPositionY(player.getPositionY() + player.getDrawImage().getHeight());
        levelView.addAsset(plateform);
        obstacles.add(plateform);
    }

    private void createEndPlateform() {
        endFlag = new EndAsset();
        endFlag.setPositionX(levelView.getWidth() / 2.f);
        endFlag.setPositionY(levelView.getHeight() / 2.f);
        endFlag.setDrawImage(Utils.getBitmapFromVectorDrawable(levelView.getResources().getDrawable(R.drawable.ic_start)));
        levelView.addAsset(endFlag);

        final Asset plateform = createPlatform(endFlag.getDrawImage().getWidth());
        plateform.setPositionX(endFlag.getPositionX());
        plateform.setPositionY(endFlag.getPositionY() + endFlag.getDrawImage().getHeight());
        levelView.addAsset(plateform);
        obstacles.add(plateform);
    }

    private Asset createPlatform(int length) {
        final Asset platform = new ObstacleAsset();
        Rect rectImage = new Rect(0, 0, length, 5);
        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setAntiAlias(true);
        p.setFilterBitmap(true);
        p.setDither(true);
        p.setColor(Color.BLACK);

        Bitmap bitmap = Bitmap.createBitmap(rectImage.width(), rectImage.height(), Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(bitmap);
        c.drawRect(rectImage.left, rectImage.top, rectImage.right,
                rectImage.bottom, p);

        platform.setDrawImage(bitmap);

        return platform;
    }

    /**
     * Mouvement du joueur vers l'avant
     */
    public void movePlayerForward() {
        if (player == null) {
            return;
        }
        Objects.requireNonNull(player).setWalking(true);
    }

    /**
     * arrêt du joueur
     */
    public void stopPlayerMovement() {
        if (player == null) {
            return;
        }
        Objects.requireNonNull(player).setWalking(false);
    }

    /**
     * Saut du joueur
     */
    public void startPlayerJump() {
        if (player == null) {
            return;
        }
        if (Objects.requireNonNull(player).canJump()) {
            Objects.requireNonNull(player).jump();
        }
    }

    /**
     * Quand on touche l'obstacle.
     *
     * @return true si on touche l'obstacle, false sinon
     */
    public boolean isOnObstacle() {
        if(player != null) {
            Rect userPos = Objects.requireNonNull(player).myPositionToRect();
            userPos.bottom += 1;
            for (final Asset asset : obstacles) {
                if (asset.isOnHitBox(userPos)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isNextToRightWall() {
        if(player != null) {
            Rect userPos = Objects.requireNonNull(player).myPositionToRect();
            userPos.right += 1;
            for (final Asset asset : obstacles) {
                if (asset.isOnHitBox(userPos)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isTouchEndFlag() {
        if (endFlag != null && player != null && endFlag.isOnHitBox(player.myPositionToRect())) {
            player.killPlayer();
            GameActivity gameActivity = (GameActivity) levelView.getContext();
            gameActivity.switchToMainActivity(playerNumberTurn);
            return true;
        }
        return false;
    }

    public boolean isOnDeathZone() {
        if (player != null && (player.getPositionX() > levelView.getWidth() || player.getPositionY() > levelView.getHeight())) {
            player.killPlayer();
            player = null;
            GameActivity gameActivity = (GameActivity) levelView.getContext();
            gameActivity.switchToMainActivity(playerNumberTurn);
        }
        return false;
    }


}
