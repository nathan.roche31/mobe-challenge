package helloandroid.m2dl.mobe_challenge.ui.game;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import helloandroid.m2dl.mobe_challenge.MainActivity;
import helloandroid.m2dl.mobe_challenge.R;
import helloandroid.m2dl.mobe_challenge.ui.photoActivity.PhotoActivity;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class GameActivity extends AppCompatActivity {

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        Objects.requireNonNull(getSupportActionBar()).hide();
        int playerNumber = (int) Objects.requireNonNull(getIntent().getExtras()).get("playerNumber");

        setContentView(R.layout.activity_game);
        LevelView levelView = findViewById(R.id.level);
        levelView.setPlayerNumber(playerNumber);
    }

    public void switchToMainActivity(int playerNumber) {
        final Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("playerNumber", playerNumber);
        startActivity(intent);
    }

    public void switchToPhotoActivity(int playerNumber) {
        final Intent intent = new Intent(this, PhotoActivity.class);
        intent.putExtra("playerNumber", playerNumber);
        startActivity(intent);
    }
}
